//Demo script for job applications: web scraper with NodeJS
//Scraping attributes may vary from page to page, but here we are using a demo page found at https://books.toscrape.com/

const cheerio = require ("cheerio");
const axios = require ("axios");
const j2cp = require ("json2csv").Parser;
const fs = require ("fs"); //Initializing filesystem

//Page to scrape
const url = "https://books.toscrape.com/catalogue/category/books/mystery_3/index.html";
const baseUrl = "https://books.toscrape.com/catalogue/category/books/mystery_3/"; //// This will be used to find data spread in multiple pages articles
const bookData = []; //Scraped data to be stored in csv file (or DB)

//Sendind request to page with an asyc function
async function getBooks()
{
	try
	{
		const response = await axios.get(url); //No need to deal with headers for this example
		const $=cheerio.load(response.data); //No need to give a name to the var
		
		//Using each() method to perform a loop, then find method, then text method.
		const books = $("article");
		books.each(function()
		{
			title = $(this).find("h3 a").text();
			price = $(this).find(".price_color").text(); //These tags depend on page structure
			stock = $(this).find(".availability").text().trim(); //Adding trim method for readability in generated csv: removes new lines for example
			
			bookData.push({title, price, stock});
		});
		
		/*Fixing data spread in multiple pages -> skipping fix for now, example has errors
		if ($(".next a").length > 0 )
		{
			nextPage = baseUrl + $(".next a").attr("href");
			getBooks(nextPage);
		}
		else{
			
		}*/
		
		const parser = new j2cp();
		const csv = parser.parse(bookData);
		fs.writeFileSync("./books.csv", csv); //Writing data to folder, file name books.csv
		
		console.log(bookData);
	}
	catch(error)
	{
		console.error(error);
	}
}

getBooks(url); //Let's scrape