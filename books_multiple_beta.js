//Demo script for job applications: web scraper with NodeJS - Extracting data automatically from web sites
//html tags (h1, h2, etc.) may vary from page to page, but here we are using a demo page found at https://books.toscrape.com/
//Uses cheerio, axios and j2cp libraries

const cheerio = require ("cheerio");
const axios = require ("axios");
const j2cp = require ("json2csv").Parser;
const fs = require ("fs"); //Initializing filesystem

//Page to scrape
const targetUrl = "https://books.toscrape.com/catalogue/category/books/mystery_3/index.html";
const baseUrl = "https://books.toscrape.com/catalogue/category/books/mystery_3/"; //This will be used to find data spread in multiple pages articles
const bookData = []; //Scraped data to be stored in csv file (or DB), as a table

//Sendind request to page with an async function
async function getBooks(String)
{
	try
	{
		const response = await axios.get(String); //No need to deal with headers for this example
		const $=cheerio.load(response.data); //No need to give a name to the var
		
		//Using each() method to perform a loop, then find() method, then text() method.
		const books = $("article");
		books.each(function()
		{
			//Using Caps just because the var names will be written to file as column names
			Title = $(this).find("h3 a").text(); //Finding text int h3 tag element: in this page, the title
			Price = $(this).find(".price_color").text(); //These tags depend on page structure, but there is the price in this page
			Stock = $(this).find(".availability").text().trim(); //Adding trim method for readability in generated csv: removes new lines for example
			
			bookData.push({Title, Price, Stock}); //Pushing data in table
		});
		
		/*Fixing data spread in several pages*/
		if ($(".next a").length > 0 )
		{
			nextPage = baseUrl + $(".next a").attr("href"); //We are getting the href link from the a element of .next class in html file (this class contains next button)
			getBooks(nextPage); //Rerun function on next page
		}
		//We reached last next button, we can now write data
		else
		{
			const parser = new j2cp(); //New object using j2cp
			const csv = parser.parse(bookData); //j2cp parse() method on csv
			fs.writeFileSync("./books.csv", csv); //Writing data to folder, file name books.csv
		}
		
		console.log(bookData);
	}
	catch(error)
	{
		console.error(error); //The try()/catch() method expects an error case
	}
}

getBooks(targetUrl); //Let's scrape