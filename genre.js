//Demo script for job applications: web scraper with NodeJS
//Scraping attributes may vary from page to page, but here we are using a demo page found at https://books.toscrape.com/

const cheerio = require ("cheerio");
const axios = require ("axios");

//Page to scrape
const url = "https://books.toscrape.com/catalogue/category/books/mystery_3/index.html";

//Sendind request to page with an asyc function
async function getGenre()
{
	try
	{
		const response = await axios.get(url); //No need to deal with headers for this example
		const $=cheerio.load(response.data); //No need to give a name to the var
		
		const genre = $("h1").text(); //Using text() method
		
		console.log(genre);
	}
	catch(error)
	{
		console.error(error);
	}
}

getGenre(url);