# NodeJS web scraper

This module runs with NodeJS. See here for NodeJS installation: https://nodejs.org/en/download/.  
It extracts info from a given web page and stores it into a csv file.  
Planned to also be able to detect new changes in the file, and send a mail in this case.  
Runs with command: ```node books_multiple.js```  
- [ ] [No license](https://choosealicense.com/no-permission/)  
